#region File Description
//-----------------------------------------------------------------------------
// Game1.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace DrawUserPrimitivesWindows
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    { //Matrices
        Matrix worldMatrix;
        Matrix viewMatrix;
        Matrix projectionMatrix;
        Matrix rotationMatrix, scaleMatrix, translationMatrix, lookAtMatrix, fullMatrix, projMatrix, finalImageMatrix;

        // vectors
        Vector3 up = Vector3.Up;
        Vector3 forward = Vector3.Forward;
        Vector3 right = Vector3.Right;


        Vector3 axis;


        BasicEffect basicEffect;
        VertexDeclaration vertexDeclaration;
        VertexPositionColor[] pointList;

        List<VertexPositionColor> vertexList;
        List<int> indexList;

        //cube points

        Vector3 point0 = new Vector3(-1, -1, 1);
        Vector3 point1 = new Vector3(-1, -1, -1);

        Vector3 point2 = new Vector3(1, -1, 1);
        Vector3 point3 = new Vector3(1, -1, -1);

        Vector3 point4 = new Vector3(-1, 1, 1);
        Vector3 point5 = new Vector3(-1, 1, -1);

        Vector3 point6 = new Vector3(1, 1, 1);
        Vector3 point7 = new Vector3(1, 1, -1);

        VertexBuffer vertexBuffer;

        int points = 8, count = 1;  ////  ????

        RasterizerState rasterizerState;

        GraphicsDeviceManager graphics;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
 
        protected override void Initialize()
        {
            setLookAtMatrix();
            InitializeTransform();
            InitializeEffect();
            
            /*---------------------------------------------------
             *  Uncomment one of the options to see the 
             *  different versions of how to get
             *  final image
             *  
             *  Option 1 uses the combined matrix from multiplying
             *  rotatition, scal and translation then multiplying
             *  view and projection to get one final matrix to 
             *  transform the cube
             *  --------------------------------------------------
             *  Option 2 changes the cube points tranforming by one
             *  matrix at a time. 
             *  rotation tranforms cube 
             *  -> scale transform rotation cube
             *  -> translation tranform scale cube 
             *  -> viewing tranform translation cube
             *  -> projection matrix tranfrom viewing cube
             *  -> final image
             *  
             * --------------------------------------------------
             */

            // uncomment 1 option and run

            //-------Option 1-----comment out if choosing another option
            
            setFinalImageMatrixOption1();



            //-------Option 2-----comment out if choosing another option
           
            //setFinalImageMatrixOption2();



            
            InitializeTriangleList();
            
            rasterizerState = new RasterizerState();
            rasterizerState.FillMode = FillMode.Solid;
            rasterizerState.CullMode = CullMode.None;

            base.Initialize();
        }   
        protected override void LoadContent()
        {

            // TODO: use this.Content to load your game content here
        }
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }


        
        private void InitializeTransform()
        {

            setLookAtMatrix();
            viewMatrix = lookAtMatrix;

            //projectionMatrix = projMatrix;
            projectionMatrix = 
                Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(90),
                GraphicsDevice.Viewport.AspectRatio, 1, 1000);
            
         
        }

        
        
       
        private void setProjectionMatrix() {
           
          
            // Projection Matrix
            projMatrix = new Matrix(1, 0, 0, 0,
                                    0, 1, 0, 0,
                                    0, 0, 1, -1,
                                    0, 0, 0, 0);
           
        }


        private void setFinalImageMatrixOption1() {

            Console.WriteLine("\n Option 1 uses the combined matrix from multiplying"
                               + "\n rotatition, scal and translation then multiplying"
                               + "\n view and projection to get one final matrix to "
                               + "\n transform the cube");
            
            
            setSingleMatrixRotateScaleTranslate();
            Console.WriteLine("\n ROTATION SCALE AND TRANSLATION \n"+matrixOutput(fullMatrix));

            setLookAtMatrix();
            Console.WriteLine("\n LOOK AT \n "+ matrixOutput(lookAtMatrix));

            //setProjectionMatrix();
            //projMatrix = projectionMatrix;
            Console.WriteLine("\n PROJECTION \n" + matrixOutput(projectionMatrix));


            finalImageMatrix = fullMatrix * lookAtMatrix *projectionMatrix;
            Console.WriteLine("\n FINAL Matrix  \n" +matrixOutput(finalImageMatrix));


            point0 = Vector3.Transform(point0, finalImageMatrix);
            point1 = Vector3.Transform(point1, finalImageMatrix);
            point2 = Vector3.Transform(point2, finalImageMatrix);
            point3 = Vector3.Transform(point3, finalImageMatrix);
            point4 = Vector3.Transform(point4, finalImageMatrix);
            point5 = Vector3.Transform(point5, finalImageMatrix);
            point6 = Vector3.Transform(point6, finalImageMatrix);
            point7 = Vector3.Transform(point7, finalImageMatrix);

            vertexList = new List<VertexPositionColor>();

            Console.WriteLine("\n FINAL IMAGE \n");
            pointOutput();

            vertexList.Add(new VertexPositionColor(point0, Color.Red));
            vertexList.Add(new VertexPositionColor(point1, Color.Black));
            vertexList.Add(new VertexPositionColor(point2, Color.Red));
            vertexList.Add(new VertexPositionColor(point3, Color.Black));
            vertexList.Add(new VertexPositionColor(point4, Color.Red));
            vertexList.Add(new VertexPositionColor(point5, Color.Black));
            vertexList.Add(new VertexPositionColor(point6, Color.Red));
            vertexList.Add(new VertexPositionColor(point7, Color.Black));

            
            // Initialize the vertex buffer, allocating memory for each vertex.
            vertexBuffer = new VertexBuffer(graphics.GraphicsDevice, vertexDeclaration,
                points, BufferUsage.None);

            // Set the vertex buffer data to the array of vertices.
            vertexBuffer.SetData<VertexPositionColor>(vertexList.ToArray());
        
        }
        private void setFinalImageMatrixOption2() {

            Console.WriteLine("\n Option 2 changes the cube points tranforming by one"
             + "\n matrix at a time"
             + "\n rotation tranforms cube"
             + "\n -> scale transform rotation cube"
             + "\n -> translation tranform scale cube"
             + "\n -> viewing tranform translation cube"
             + "\n -> projection matrix tranfrom viewing cube"
             + "\n -> final image");
             
            
            setRotation();
            Console.WriteLine("\n ROTATION \n" + matrixOutput(rotationMatrix));
            point0 = Vector3.Transform(point0, rotationMatrix);
            point1 = Vector3.Transform(point1, rotationMatrix);
            point2 = Vector3.Transform(point2, rotationMatrix);
            point3 = Vector3.Transform(point3, rotationMatrix);
            point4 = Vector3.Transform(point4, rotationMatrix);
            point5 = Vector3.Transform(point5, rotationMatrix);
            point6 = Vector3.Transform(point6, rotationMatrix);
            point7 = Vector3.Transform(point7, rotationMatrix);

            Console.WriteLine("After Rotation Image");
            pointOutput();

            setScale();
            Console.WriteLine("\n SCALE \n" + matrixOutput(scaleMatrix));
            point0 = Vector3.Transform(point0, scaleMatrix);
            point1 = Vector3.Transform(point1, scaleMatrix);
            point2 = Vector3.Transform(point2, scaleMatrix);
            point3 = Vector3.Transform(point3, scaleMatrix);
            point4 = Vector3.Transform(point4, scaleMatrix);
            point5 = Vector3.Transform(point5, scaleMatrix);
            point6 = Vector3.Transform(point6, scaleMatrix);
            point7 = Vector3.Transform(point7, scaleMatrix);
            
            Console.WriteLine("After Scale Image");
            pointOutput();

            setTranslation();
            Console.WriteLine("\n TRANSLATION \n" + matrixOutput(translationMatrix));
            point0 = Vector3.Transform(point0, translationMatrix);
            point1 = Vector3.Transform(point1, translationMatrix);
            point2 = Vector3.Transform(point2, translationMatrix);
            point3 = Vector3.Transform(point3, translationMatrix);
            point4 = Vector3.Transform(point4, translationMatrix);
            point5 = Vector3.Transform(point5, translationMatrix);
            point6 = Vector3.Transform(point6, translationMatrix);
            point7 = Vector3.Transform(point7, translationMatrix);
            
            Console.WriteLine("After Translation Image");
            pointOutput();

            setLookAtMatrix();
            Console.WriteLine("\n Viewing Matrix \n" + matrixOutput(lookAtMatrix));
            point0 = Vector3.Transform(point0, lookAtMatrix);
            point1 = Vector3.Transform(point1, lookAtMatrix);
            point2 = Vector3.Transform(point2, lookAtMatrix);
            point3 = Vector3.Transform(point3, lookAtMatrix);
            point4 = Vector3.Transform(point4, lookAtMatrix);
            point5 = Vector3.Transform(point5, lookAtMatrix);
            point6 = Vector3.Transform(point6, lookAtMatrix);
            point7 = Vector3.Transform(point7, lookAtMatrix);
           
            Console.WriteLine("After Viewing Image");
            pointOutput();

            Console.WriteLine("\n Projection Matrix \n" + matrixOutput(projectionMatrix));
            point0 = Vector3.Transform(point0, projectionMatrix);
            point1 = Vector3.Transform(point1, projectionMatrix);
            point2 = Vector3.Transform(point2, projectionMatrix);
            point3 = Vector3.Transform(point3, projectionMatrix);
            point4 = Vector3.Transform(point4, projectionMatrix);
            point5 = Vector3.Transform(point5, projectionMatrix);
            point6 = Vector3.Transform(point6, projectionMatrix);
            point7 = Vector3.Transform(point7, projectionMatrix);


            Console.WriteLine("\n FINAL IMAGE \n");
            pointOutput();
            vertexList = new List<VertexPositionColor>();

            vertexList.Add(new VertexPositionColor(point0, Color.Red));
            vertexList.Add(new VertexPositionColor(point1, Color.Black));
            vertexList.Add(new VertexPositionColor(point2, Color.Red));
            vertexList.Add(new VertexPositionColor(point3, Color.Black));
            vertexList.Add(new VertexPositionColor(point4, Color.Red));
            vertexList.Add(new VertexPositionColor(point5, Color.Black));
            vertexList.Add(new VertexPositionColor(point6, Color.Red));
            vertexList.Add(new VertexPositionColor(point7, Color.Black));

            
            // Initialize the vertex buffer, allocating memory for each vertex.
            vertexBuffer = new VertexBuffer(graphics.GraphicsDevice, vertexDeclaration,
                points, BufferUsage.None);

            // Set the vertex buffer data to the array of vertices.
            vertexBuffer.SetData<VertexPositionColor>(vertexList.ToArray());
        
        }
       


        // Sets the lookAt matrix 
        private void setLookAtMatrix()
        {

            lookAtMatrix = Matrix.CreateLookAt(new Vector3(13, 5, 52),
                                               new Vector3(2, 11, 2),
                                               Vector3.Normalize(new Vector3(3, 2, 11)));


        }
        private void afterLookAt()
        {
            setLookAtMatrix();

            Console.WriteLine("-------------------LOOK AT MATRIX-------------------");
            Console.WriteLine(lookAtMatrix.ToString());
            Console.WriteLine("-------------------CUBE AFTER LOOK AT-------------------");
            point0 = Vector3.Transform(point0, lookAtMatrix);
            Console.WriteLine("POINT 0: " + point0.ToString());

            point1 = Vector3.Transform(point1, lookAtMatrix);
            Console.WriteLine("POINT 1: " + point1.ToString());

            point2 = Vector3.Transform(point2, lookAtMatrix);
            Console.WriteLine("POINT 2: " + point2.ToString());

            point3 = Vector3.Transform(point3, lookAtMatrix);
            Console.WriteLine("POINT 3: " + point3.ToString());

            point4 = Vector3.Transform(point4, lookAtMatrix);
            Console.WriteLine("POINT 4: " + point4.ToString());

            point5 = Vector3.Transform(point5, lookAtMatrix);
            Console.WriteLine("POINT 5: " + point5.ToString());

            point6 = Vector3.Transform(point6, lookAtMatrix);
            Console.WriteLine("POINT 6: " + point6.ToString());

            point7 = Vector3.Transform(point7, lookAtMatrix);
            Console.WriteLine("POINT 7: " + point7.ToString());


          
            vertexList = new List<VertexPositionColor>();
            vertexList.Add(new VertexPositionColor(point0, Color.Red)); // 20
            vertexList.Add(new VertexPositionColor(point1, Color.Black));

            vertexList.Add(new VertexPositionColor(point2, Color.Red)); // 20
            vertexList.Add(new VertexPositionColor(point3, Color.Black));

            vertexList.Add(new VertexPositionColor(point4, Color.Red)); // 20
            vertexList.Add(new VertexPositionColor(point5, Color.Black));

            vertexList.Add(new VertexPositionColor(point6, Color.Red)); // 20
            vertexList.Add(new VertexPositionColor(point7, Color.Black));

            // Initialize the vertex buffer, allocating memory for each vertex.
            vertexBuffer = new VertexBuffer(graphics.GraphicsDevice, vertexDeclaration,
                points, BufferUsage.None);

            // Set the vertex buffer data to the array of vertices.
            vertexBuffer.SetData<VertexPositionColor>(vertexList.ToArray());


        }
        private void setAxis()
        {
            axis = Vector3.Normalize(new Vector3(11, 2, 2));
        }


        // Sets Translation matrix
        private void setTranslation()
        {
            translationMatrix = new Matrix(1, 0, 0, 0, 
                                            0, 1, 0, 0,
                                            0, 0, 1, 0,
                                            0, 0, 3, 1);
            //translationMatrix = Matrix.CreateTranslation(0,0,3);
            
        }
        private void afterTranslation()
        {
            setTranslation();

            Console.WriteLine("-------------------TRANSLATION MATRIX-------------------");
            Console.WriteLine(translationMatrix.ToString());

            Console.WriteLine("-------------------CUBE AFTER SCALE-------------------");
            point0 = Vector3.Transform(point0, translationMatrix);
            Console.WriteLine("POINT 0: " + point0.ToString());

            point1 = Vector3.Transform(point1, translationMatrix);
            Console.WriteLine("POINT 1: " + point1.ToString());

            point2 = Vector3.Transform(point2, translationMatrix);
            Console.WriteLine("POINT 2: " + point2.ToString());

            point3 = Vector3.Transform(point3, translationMatrix);
            Console.WriteLine("POINT 3: " + point3.ToString());

            point4 = Vector3.Transform(point4, translationMatrix);
            Console.WriteLine("POINT 4: " + point4.ToString());

            point5 = Vector3.Transform(point5, translationMatrix);
            Console.WriteLine("POINT 5: " + point5.ToString());

            point6 = Vector3.Transform(point6, translationMatrix);
            Console.WriteLine("POINT 6: " + point6.ToString());

            point7 = Vector3.Transform(point7, translationMatrix);
            Console.WriteLine("POINT 7: " + point7.ToString());

            vertexList = new List<VertexPositionColor>();

            vertexList.Add(new VertexPositionColor(point0, Color.Red));
            vertexList.Add(new VertexPositionColor(point1, Color.Black));

            vertexList.Add(new VertexPositionColor(point2, Color.Red));
            vertexList.Add(new VertexPositionColor(point3, Color.Black));

            vertexList.Add(new VertexPositionColor(point4, Color.Red));
            vertexList.Add(new VertexPositionColor(point5, Color.Black));

            vertexList.Add(new VertexPositionColor(point6, Color.Red));
            vertexList.Add(new VertexPositionColor(point7, Color.Black));

            // Initialize the vertex buffer, allocating memory for each vertex.
            vertexBuffer = new VertexBuffer(graphics.GraphicsDevice, vertexDeclaration,
                points, BufferUsage.None);

            // Set the vertex buffer data to the array of vertices.
            vertexBuffer.SetData<VertexPositionColor>(vertexList.ToArray());

        }



        // Sets rotation matrix
        private void setRotation()
        {
            setAxis();
            rotationMatrix = Matrix.CreateFromAxisAngle(axis, MathHelper.ToRadians(5));



            /*rotationMatrix = new Matrix(0.992f, 0.08676f, -0.0871f, 0,
                                        -0.0792f, 0.9929f, 0.087f, 0,
                                        0.094f, -0.0792f, 0.992f, 0,
                                        0, 0, 0, 1);*/
        }
        private void afterRotation()
        {

            setRotation();
            Console.WriteLine("-------------------ROTATION MATRIX-------------------");
            Console.WriteLine(rotationMatrix.ToString());

            Console.WriteLine("-------------------CUBE AFTER SCALE-------------------");

            point0 = Vector3.Transform(point0, rotationMatrix);
            Console.WriteLine(point0.ToString());

            point1 = Vector3.Transform(point1, rotationMatrix);
            Console.WriteLine(point1.ToString());

            point2 = Vector3.Transform(point2, rotationMatrix);
            Console.WriteLine(point2.ToString());

            point3 = Vector3.Transform(point3, rotationMatrix);
            Console.WriteLine(point3.ToString());

            point4 = Vector3.Transform(point4, rotationMatrix);
            Console.WriteLine(point4.ToString());

            point5 = Vector3.Transform(point5, rotationMatrix);
            Console.WriteLine(point5.ToString());

            point6 = Vector3.Transform(point6, rotationMatrix);
            Console.WriteLine(point6.ToString());

            point7 = Vector3.Transform(point7, rotationMatrix);
            Console.WriteLine(point7.ToString());


            vertexList = new List<VertexPositionColor>();


            vertexList.Add(new VertexPositionColor(point0, Color.Red)); // 20
            vertexList.Add(new VertexPositionColor(point1, Color.Black));

            vertexList.Add(new VertexPositionColor(point2, Color.Red)); // 20
            vertexList.Add(new VertexPositionColor(point3, Color.Black));

            vertexList.Add(new VertexPositionColor(point4, Color.Red)); // 20
            vertexList.Add(new VertexPositionColor(point5, Color.Black));

            vertexList.Add(new VertexPositionColor(point6, Color.Red)); // 20
            vertexList.Add(new VertexPositionColor(point7, Color.Black));

            // Initialize the vertex buffer, allocating memory for each vertex.
            vertexBuffer = new VertexBuffer(graphics.GraphicsDevice, vertexDeclaration,
                points, BufferUsage.None);

            // Set the vertex buffer data to the array of vertices.
            vertexBuffer.SetData<VertexPositionColor>(vertexList.ToArray());

        }



        // Sets the scale matrix
        private void setScale()
        {
            scaleMatrix = new Matrix(11, 0, 0, 0,
                                              0, 2, 0, 0,
                                              0, 0, 2, 0,
                                              0, 0, 0, 1);


            // scaleMatrix = Matrix.CreateScale(11, 2, 2);
        }
        private void afterScaling()
        {

            setScale();

            Console.WriteLine("-------------------SCALING MATRIX-------------------");
            Console.WriteLine(scaleMatrix.ToString());
            Console.WriteLine("-------------------CUBE AFTER SCALE-------------------");
            point0 = Vector3.Transform(point0, scaleMatrix);
            Console.WriteLine("POINT 0: " + point0.ToString());

            point1 = Vector3.Transform(point1, scaleMatrix);
            Console.WriteLine("POINT 1: " + point1.ToString());

            point2 = Vector3.Transform(point2, scaleMatrix);
            Console.WriteLine("POINT 2: " + point2.ToString());

            point3 = Vector3.Transform(point3, scaleMatrix);
            Console.WriteLine("POINT 3: " + point3.ToString());

            point4 = Vector3.Transform(point4, scaleMatrix);
            Console.WriteLine("POINT 4: " + point4.ToString());

            point5 = Vector3.Transform(point5, scaleMatrix);
            Console.WriteLine("POINT 5: " + point5.ToString());

            point6 = Vector3.Transform(point6, scaleMatrix);
            Console.WriteLine("POINT 6: " + point6.ToString());

            point7 = Vector3.Transform(point7, scaleMatrix);
            Console.WriteLine("POINT 7: " + point7.ToString());


            vertexList = new List<VertexPositionColor>();
            vertexList.Add(new VertexPositionColor(point0, Color.Red)); // 20
            vertexList.Add(new VertexPositionColor(point1, Color.Black));

            vertexList.Add(new VertexPositionColor(point2, Color.Red)); // 20
            vertexList.Add(new VertexPositionColor(point3, Color.Black));

            vertexList.Add(new VertexPositionColor(point4, Color.Red)); // 20
            vertexList.Add(new VertexPositionColor(point5, Color.Black));

            vertexList.Add(new VertexPositionColor(point6, Color.Red)); // 20
            vertexList.Add(new VertexPositionColor(point7, Color.Black));

            // Initialize the vertex buffer, allocating memory for each vertex.
            vertexBuffer = new VertexBuffer(graphics.GraphicsDevice, vertexDeclaration,
                points, BufferUsage.None);

            // Set the vertex buffer data to the array of vertices.
            vertexBuffer.SetData<VertexPositionColor>(vertexList.ToArray());
        }


     
        private void setSingleMatrixRotateScaleTranslate()
        {
            setTranslation();
            setScale();
            setRotation();
            
            fullMatrix = rotationMatrix
                          * scaleMatrix
                          * translationMatrix;
            
            
        }
        private void afterSingleMatrixRotateScaleTranslate()
        {
            setSingleMatrixRotateScaleTranslate();
            Console.WriteLine("-------------------FULL TRANSFORMATION-------------------");
            Console.WriteLine(fullMatrix.ToString());

            Console.WriteLine("-------------------CUBE AFTER SCALE ROTATION TRANSLATION-------------------");
            point0 = Vector3.Transform(point0, fullMatrix);
            Console.WriteLine("POINT 0: " + point0.ToString());

            point1 = Vector3.Transform(point1, fullMatrix);
            Console.WriteLine("POINT 1: " + point1.ToString());

            point2 = Vector3.Transform(point2, fullMatrix);
            Console.WriteLine("POINT 2: " + point2.ToString());

            point3 = Vector3.Transform(point3, fullMatrix);
            Console.WriteLine("POINT 3: " + point3.ToString());

            point4 = Vector3.Transform(point4, fullMatrix);
            Console.WriteLine("POINT 4: " + point4.ToString());

            point5 = Vector3.Transform(point5, fullMatrix);
            Console.WriteLine("POINT 5: " + point5.ToString());

            point6 = Vector3.Transform(point6, fullMatrix);
            Console.WriteLine("POINT 6: " + point6.ToString());

            point7 = Vector3.Transform(point7, fullMatrix);
            Console.WriteLine("POINT 7: " + point7.ToString());

            vertexList = new List<VertexPositionColor>();

            vertexList.Add(new VertexPositionColor(point0, Color.Red));
            vertexList.Add(new VertexPositionColor(point1, Color.Black));
            vertexList.Add(new VertexPositionColor(point2, Color.Red));
            vertexList.Add(new VertexPositionColor(point3, Color.Black));
            vertexList.Add(new VertexPositionColor(point4, Color.Red));
            vertexList.Add(new VertexPositionColor(point5, Color.Black));
            vertexList.Add(new VertexPositionColor(point6, Color.Red));
            vertexList.Add(new VertexPositionColor(point7, Color.Black));


            // Initialize the vertex buffer, allocating memory for each vertex.
            vertexBuffer = new VertexBuffer(graphics.GraphicsDevice, vertexDeclaration,
                points, BufferUsage.None);

            // Set the vertex buffer data to the array of vertices.
            vertexBuffer.SetData<VertexPositionColor>(vertexList.ToArray());

        }



        private void pointOutput() {
            Console.WriteLine("POINT 0: " + point0.ToString());
            Console.WriteLine("POINT 1: " + point1.ToString());
            Console.WriteLine("POINT 2: " + point2.ToString());
            Console.WriteLine("POINT 3: " + point3.ToString());
            Console.WriteLine("POINT 4: " + point4.ToString());
            Console.WriteLine("POINT 5: " + point5.ToString());
            Console.WriteLine("POINT 6: " + point6.ToString());
            Console.WriteLine("POINT 7: " + point7.ToString());
        }
        private string matrixOutput(Matrix matrix) {

            string text;
            text = "Matrix List:\n" + matrix.M11 + ", " +
                                    matrix.M12 + ", " +
                                    matrix.M13 + ", " +
                                    matrix.M14 + ",\n" +
                                    matrix.M21 + " " +
                                    matrix.M22 + ", " +
                                    matrix.M23 + ", " +
                                    matrix.M24 + ",\n" +
                                    matrix.M31 + " " +
                                    matrix.M32 + ", " +
                                    matrix.M33 + ", " +
                                    matrix.M34 + ",\n" +
                                    matrix.M41 + " " +
                                    matrix.M42 + ", " +
                                    matrix.M43 + ", " +
                                    matrix.M44 +
                                         "\n--------------------------------";

            return text;
        
        }





        private void InitializeEffect()
        {

            vertexDeclaration = new VertexDeclaration(new VertexElement[]
                {
                    new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
                    new VertexElement(12, VertexElementFormat.Color, VertexElementUsage.Color, 0)
                }
            );

            basicEffect = new BasicEffect(GraphicsDevice);
            basicEffect.VertexColorEnabled = true;

            worldMatrix = Matrix.Identity;
            basicEffect.World = worldMatrix;
            basicEffect.View = viewMatrix;
            basicEffect.Projection = projectionMatrix;
        }
        private void InitializePoints()
        {
            vertexList = new List<VertexPositionColor>();
            vertexList.Add(new VertexPositionColor(point0, Color.Blue)); // 20
            vertexList.Add(new VertexPositionColor(point1, Color.Black));

            vertexList.Add(new VertexPositionColor(point2, Color.Green)); // 20
            vertexList.Add(new VertexPositionColor(point3, Color.Black));

            vertexList.Add(new VertexPositionColor(point4, Color.Red)); // 20
            vertexList.Add(new VertexPositionColor(point5, Color.Black));

            vertexList.Add(new VertexPositionColor(point6, Color.Yellow)); // 20
            vertexList.Add(new VertexPositionColor(point7, Color.Black));
           

            // Initialize the vertex buffer, allocating memory for each vertex.
            vertexBuffer = new VertexBuffer(graphics.GraphicsDevice, vertexDeclaration,
                points, BufferUsage.None);

            // Set the vertex buffer data to the array of vertices.
            vertexBuffer.SetData<VertexPositionColor>(vertexList.ToArray());
        }
        private void InitializeTriangleList()
        {


            indexList = new List<int>();
            
            //front face
            indexList.Add(0);
            indexList.Add(2);
            indexList.Add(6);
            indexList.Add(0);
            indexList.Add(6);
            indexList.Add(4);

            //back face
            indexList.Add(1);
            indexList.Add(5);
            indexList.Add(7);
            indexList.Add(1);
            indexList.Add(7);
            indexList.Add(3);

            //base face
            indexList.Add(0);
            indexList.Add(1);
            indexList.Add(3);
            indexList.Add(0);
            indexList.Add(3);
            indexList.Add(2);

            //right face
            indexList.Add(2);
            indexList.Add(7);
            indexList.Add(6);
            indexList.Add(2);
            indexList.Add(3);
            indexList.Add(7);

            //left face
            indexList.Add(0);
            indexList.Add(4);
            indexList.Add(5);
            indexList.Add(0);
            indexList.Add(5);
            indexList.Add(1);

            //top face
            indexList.Add(6);
            indexList.Add(5);
            indexList.Add(4);
            indexList.Add(6);
            indexList.Add(7);
            indexList.Add(5);



        }
        private void DrawTriangleList()
        {
            GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(
                PrimitiveType.TriangleList,
                vertexList.ToArray(),
                0,   // vertex buffer offset to add to each element of the index buffer
                vertexList.Count(),   // number of vertices to draw
                indexList.ToArray(),
                0,   // first index element to read
                indexList.Count() / 3    // number of primitives to draw
            );
        }
        
       
        protected override void Update(GameTime gameTime)
        {
           // setAxis();

            Matrix m = Matrix.CreateFromAxisAngle(axis, MathHelper.ToRadians(5));

            up = Vector3.Transform(up, m);
            right = Vector3.Transform(right, m);
            forward = Vector3.Transform(forward, m);
            KeyboardState k = Keyboard.GetState();


            //afterFullTransformation();


            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
#if WINDOWS

            if (Keyboard.GetState().IsKeyDown(Keys.Space) && count == 0)
            {
                afterSingleMatrixRotateScaleTranslate(); InitializeTriangleList(); DrawTriangleList();
                Console.WriteLine("full transformation done");
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Space) && count == 1)
            {
                afterRotation(); InitializeTriangleList(); DrawTriangleList();
                Console.WriteLine("rotation done");
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Space) && count == 0)
            {
                afterScaling(); InitializeTriangleList(); DrawTriangleList();
                Console.WriteLine("scaling done");
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Space) && count == 0)
            {
                afterTranslation(); InitializeTriangleList(); DrawTriangleList();
                Console.WriteLine("translation done");
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();
#endif

 

            base.Update(gameTime);
        } 
        protected override void Draw(GameTime gameTime)
        {

            GraphicsDevice.Clear(Color.SteelBlue);

            basicEffect.World = Matrix.CreateWorld(Vector3.Zero, forward, up);
    
            foreach (EffectPass pass in basicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();


                        GraphicsDevice.RasterizerState = rasterizerState;
                        DrawTriangleList();
   

            }

            base.Draw(gameTime);
        }

        
        

    }
}
